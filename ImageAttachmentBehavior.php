<?php
/**
 * Behavior to handle image associated with model
 *
 * @example
 *      'previewImageAttachmentBehavior' => array(
 *          'class' => 'ext.imageAttachment.ImageAttachmentBehavior',
 *          'previewHeight' => 200,
 *          'previewWidth' => 300,
 *          'extension' => 'jpg',
 *          'directory' => 'images/productTheme/preview',
 *          'url' => Yii::app()->request->baseUrl . '/images/productTheme/preview',
 *          'versions' => array(
 *              'small' => array(
 *                  'resize' => array(200, null),
 *              ),
 *              'medium' => array(
 *                  'resize' => array(800, null),
 *              )
 *          )
 *      )
 *
 * @author Bogdan Savluk <savluk.bogdan@gmail.com>
 *
 */
class ImageAttachmentBehavior extends CActiveRecordBehavior
{
    /**
     * Widget preview height
     * @var int
     */
    public $previewHeight;
    /**
     * Widget preview width
     * @var int
     */
    public $previewWidth;
    /**
     * Extension for saved images
     * @var string
     */
    public $extension;
    /**
     * Path to directory where to save uploaded images
     * @var string
     */
    public $directory;
    /**
     * Directory Url, without trailing slash
     * @var string
     */
    public $url;
    /**
     * @var array Settings for image auto-generation
     * @note
     * 'preview' & 'original' versions names are reserved for image preview in widget
     * and original image files
     * @example
     *  array(
     *       'small' => array(
     *              'resize' => array(200, null),
     *       ),
     *      'medium' => array(
     *              'resize' => array(800, null),
     *      )
     *  );
     */
    public $versions;

    /**
     * name of query param for modification time hash
     * to avoid using outdated version from cache - set it to false
     * @var string
     */
    public $timeHash = '_';

    public $tmpSuffix = '_tmp';

    public $modelTitle;

    private $_imageId;

    public function setRemoved ($removed, $imageId = null)
    {
        if (!$imageId)
            $imageId = $this->getImageId();

        if ($removed)
            Yii::app()->session["imageAttachment_removed_$imageId"] = true;
        else
            return Yii::app()->session->remove("imageAttachment_removed_$imageId");
    }

    public function getRemoved ($imageId = null)
    {
        if (!$imageId)
            $imageId = $this->getImageId();

        return Yii::app()->session->get("imageAttachment_removed_$imageId");
    }

    /**
     * @param CComponent $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);
        if (!isset($this->versions['original']))
            $this->versions['original'] = array();
        if (!isset($this->versions['preview']))
            $this->versions['preview'] = array('fit' => array($this->previewWidth, $this->previewHeight));
        $this->_imageId = $this->getImageId(true);
    }

    public function afterConstruct($event)
    {
        if (!$this->modelTitle)
        {
            if (method_exists($this->owner, 'title'))
                $this->modelTitle = $this->owner->title();
            else
                $this->modelTitle = Yii::t('ImageAttachmentBehavior.main', 'this');
        }
        parent::afterConstruct($event);
    }

    public function afterDelete($event)
    {
        $this->removeImages();
        $this->removed = false;
        parent::afterDelete($event);
    }

    public function afterSave($event)
    {
        $imageId = $this->getImageId();
        if ($this->setRemoved(false, $imageId))
            $this->removeImages();
        else
        {
            if (strcmp($this->_imageId, $imageId) != 0) {
                foreach ($this->versions as $version => $config) {
                    $oldPath = $this->getFilePath($version, false, null, $this->_imageId);
                    $newPath = $this->getFilePath($version, false, null, $imageId);
                    if (file_exists($oldPath)) {
                        rename($oldPath, $newPath);
                    }
                }
            }
        }
        parent::afterSave($event);
    }

    public function hasImage(&$tmp = false, $ext = null)
    {
        if ($this->removed)
            return false;

        $originalImage = $this->getFilePath('original', $tmp, $ext);
        if (file_exists($originalImage))
            return true;

        if ($tmp && $this->tmpSuffix)
        {
            $tmp = false;
            $originalImage = $this->getFilePath('original', $tmp, $ext);
            return file_exists($originalImage);
        }

        return false;
    }

    private function getFileName($version = '', $id = null, $tmp = false, $ext = null)
    {
        if ($id === null) {
            $id = $this->getImageId($tmp);
        }
        if ($ext === null) {
            $ext = $this->extension;
        }
        return $version . '/' . $id . '.' . $ext;
    }

    public function getUrl($version, $tmp = false)
    {
        if (!$this->hasImage($tmp))
            return null;

        if (!empty($this->timeHash)) {
            $time = filemtime($this->getFilePath($version, $tmp));
            $suffix = '?' . $this->timeHash . '=' . crc32($time);
        } else {
            $suffix = '';
        }

        return $this->url . '/' . $this->getFileName($version, null, $tmp) . $suffix;
    }

    private function getFilePath($version, $tmp = false, $ext = null, $id = null)
    {
        return $this->directory . '/' . $this->getFileName($version, $id, $tmp, $ext);
    }

    /**
     * Removes all images attached to model using this behavior
     */
    private function removeImages($ext = null, $tmp = false)
    {
        foreach ($this->versions as $version => $actions) {
            if (!$tmp && $this->tmpSuffix)
                $this->removeFile($this->getFilePath($version, false, $ext));
            $this->removeFile($this->getFilePath($version, true, $ext));
        }
    }

    public function cancel ()
    {
        $this->removed = false;

        if (!$this->tmpSuffix)
            return;

        $this->removeImages(null, true);
    }

    /**
     * Replace existing image by specified file
     * @param $path
     */
    public function setImage($path)
    {
        $this->removed = false;

        $this->checkDirectories();
        //create image preview for gallery manager
        foreach ($this->versions as $version => $actions) {
            /** @var Image $image */
            $image = Yii::app()->image->load($path);

            foreach ($actions as $method => $args) {
                call_user_func_array(array($image, $method), is_array($args) ? $args : array($args));
            }
            $image->save($this->getFilePath($version, true));
        }
    }


    /**
     * Regenerate image versions
     * Should be called in migration on every model after changes in versions configuration
     */
    public function updateImages($oldExt = null, $tmp = true)
    {
        $tmpParam = $tmp;
        if ($this->hasImage($tmp, $oldExt)) {
            $this->checkDirectories();
            if ($oldExt !== null) {
                $image = Yii::app()->image->load($this->getFilePath('original', $tmp, $oldExt));
                $image->save($this->getFilePath('original', $tmp));
            }
            foreach ($this->versions as $version => $actions) {
                if ($version !== 'original') {
                    $this->removeFile($this->getFilePath($version, $tmp));
                    /** @var Image $image */
                    $image = Yii::app()->image->load($this->getFilePath('original', $tmp, $oldExt));
                    foreach ($actions as $method => $args) {
                        call_user_func_array(array($image, $method), is_array($args) ? $args : array($args));
                    }
                    $image->save($this->getFilePath($version, $tmp));
                }
            }
            if ($oldExt !== null) {
                $this->removeImages($oldExt);
            }
        }

        if ($tmpParam && $tmp) // Value of $tmp can be modified only if $this->tmpSuffix set
            $this->updateImages($oldExt, false);
    }

    private function removeFile($fileName)
    {
        if (file_exists($fileName))
            @unlink($fileName);
    }


    private function getImageId($tmp = false)
    {
        $pk = $this->owner->getPrimaryKey();
        if (is_array($pk)) {
            $id = implode('_', $pk);
        } else {
            $id = $pk;
        }

        return $id . ($tmp ? $this->tmpSuffix : '');
    }

    private function checkDirectory($path)
    {
        if (!file_exists($path))
            mkdir($path, 0777);
    }

    private function checkDirectories()
    {
        if (!file_exists($this->directory)) {
            $this->checkPath();
        }

        foreach ($this->versions as $version => $actions) {
            $this->checkDirectory($this->directory . '/' . $version);
        }
    }

    private function checkPath()
    {
        $parts = explode('/', rtrim($this->directory, '/'));
        $i = 0;

        $path = implode('/', array_slice($parts, 0, count($parts) - $i));
        while (!file_exists($path)) {
            $i++;
            $path = implode('/', array_slice($parts, 0, count($parts) - $i));
        }
        $i--;
        $path = implode('/', array_slice($parts, 0, count($parts) - $i));
        while ($i >= 0) {
            mkdir($path, 0777);
            $i--;
            $path = implode('/', array_slice($parts, 0, count($parts) - $i));
//            $t = array_values($parts);
//            array_splice($t, count($parts)-$i, $i);
//            $path = implode('/', $t);
        }
    }
}
